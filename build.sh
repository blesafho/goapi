docker login --username $DOCKER_USERNAME --password $DOCKER_PASSWORD
# Build image
docker build -t blesafho/micro:goapi .

# Push container
docker push blesafho/micro:goapi