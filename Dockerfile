#FROM php:7.4-fpm-alpine
FROM registry.spbe.sangkuriang.co.id/microtester/apm:php-alpine
#RUN apt update
#RUN apt install -y git wget

#RUN apk update
#RUN apk --no-cache add curl
#RUN curl -L https://github.com/elastic/apm-agent-php/releases/download/v1.3.1/apm-agent-php_1.3.1_all.apk -o apm.apk
#RUN apk add --allow-untrusted apm.apk

COPY ./elastic-apm.ini /opt/elastic/apm-agent-php/etc/elastic-apm-custom.ini
RUN mkdir /app
WORKDIR /app
COPY src/ /app

CMD ["php", "-S", "0.0.0.0:80", "index.php"]